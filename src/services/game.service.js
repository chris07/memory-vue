class GameService {
  getHistory() {
    if (localStorage.getItem("history") !== null) {
      return localStorage.getItem("history");
    }
    return false;
  }
  registerHistory(games) {
    localStorage.setItem("history", games);
    if (
      localStorage.getItem("history") !== null &&
      localStorage.getItem("history") !== undefined
    ) {
      return true;
    }
    return false;
  }
}
  
export default new GameService();
  