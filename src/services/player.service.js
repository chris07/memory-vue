class PlayerService {
  getPlayer() {
    if (localStorage.getItem("joueur") !== null) {
      return localStorage.getItem("joueur");
    }
    return false;
  }
  setPlayer(player) {
    localStorage.setItem("joueur", player);
    if (
      localStorage.getItem("joueur") !== null &&
      localStorage.getItem("joueur") !== undefined
    ) {
      return true;
    }
    return false;
  }
}

export default new PlayerService();
