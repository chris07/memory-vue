import gameService from "../services/game.service.js";

const initialState = { games: null };

export const games = {
  namespaced: true,
  state: initialState,
  actions:{
    get({ commit }){
      if(gameService.getHistory()){
        commit('set', gameService.getHistory());
      }
      else{
        commit('setError');
      }
    },
    set({ commit },game){
      const history = JSON.parse(gameService.getHistory());
      const newGame = game;
      let newHistory = [];
      if (history !== false){
        newHistory = JSON.stringify([...history, newGame]);
      }
      else{
        newHistory = JSON.stringify([newGame]);
      }
      if(gameService.registerHistory(newHistory)){
        commit('set', newHistory);
      }
      else{
        commit('setError');
      }
    }
  },
  mutations: {
    set(state, games) {
      state.games = games;
    },
    setError(state) {
      state.games = null
    }
  }
};
