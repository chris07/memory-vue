import playerService from "../services/player.service.js";

const initialState = { player: null };

export const player = {
  namespaced: true,
  state: initialState,
  actions:{
    get({ commit }){
      if(playerService.getPlayer()){
        commit('set', playerService.getPlayer());
      }
      else{
        commit('setError');
      }
    },
    set({ commit },name){
      if(playerService.setPlayer(name)){
        commit('set', name);
      }
      else{
        commit('setError');
      }
    }
  },
  mutations: {
    set(state, name) {
      state.player = name;
    },
    setError(state) {
      state.player = null
    }
  }
};
