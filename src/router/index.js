import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/jeu',
    name: 'jeu',
    component: () => import(/* webpackChunkName: "jeu" */ '../views/Jeu.vue')
  },
  {
    path: '/contact',
    name: 'contact',
    component: () => import(/* webpackChunkName: "contact" */ '../views/Contact.vue')
  },
  {
    path: '/name',
    name: 'name',
    component: () => import(/* webpackChunkName: "jeu" */ '../views/Player.vue')
  },
  {
    path: '/history',
    name: 'history',
    component: () => import(/* webpackChunkName: "jeu" */ '../views/History.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next)=>{
  const player = localStorage.getItem("joueur");
  if ( player === undefined || player === null && to.name !== "name"){
    next({path: '/name'})
  }
  else if(to.path === "/"){
    next({path: '/name'})
  }
  else {
    next();
  }
})

export default router
